#include <tchar.h>
#include <assert.h>
#include <immintrin.h>
#include <windows.h>
#include <SetupAPI.h>
#include <Wbemcli.h>
#include <comutil.h>
#include "WinUtils.h"


#pragma comment(lib, "SetupAPI.lib")
#pragma comment(lib, "wbemuuid.lib")
#pragma comment(lib, "comsuppw.lib")

bool IsAVXSupported()
{
  __try
  {
    _mm256_zeroall();
    return true;
  }
  __except (EXCEPTION_EXECUTE_HANDLER)
  {
    return false;
  }
}

double PCFreq = 0.0;
__int64 CounterStart = 0;

void StartCounter()
{
  LARGE_INTEGER li;
  if (!QueryPerformanceFrequency(&li))
    printf("QueryPerformanceFrequency failed!\n");

  PCFreq = double(li.QuadPart) / 1000.0;

  QueryPerformanceCounter(&li);
  CounterStart = li.QuadPart;
}

double GetCounter()
{
  LARGE_INTEGER li;
  QueryPerformanceCounter(&li);
  return double(li.QuadPart - CounterStart) / PCFreq;
}

HRESULT InitializeCom()
{
  HRESULT result = CoInitializeEx(0, COINIT_APARTMENTTHREADED);
  if (FAILED(result))
    return result;
  result = CoInitializeSecurity(NULL, -1, NULL, NULL, RPC_C_AUTHN_LEVEL_DEFAULT, RPC_C_IMP_LEVEL_IMPERSONATE, NULL, EOAC_NONE, NULL);
  if (FAILED(result) && result != RPC_E_TOO_LATE)
  {
    CoUninitialize();
    return result;
  }
  return NOERROR;
}

HRESULT GetWbemService(IWbemLocator** pLocator, IWbemServices** pService)
{
  HRESULT result = CoCreateInstance(CLSID_WbemLocator, 0, CLSCTX_INPROC_SERVER,
    IID_IWbemLocator, reinterpret_cast<LPVOID*>(pLocator));

  if (FAILED(result))
  {
    return result;
  }

  result = (*pLocator)->ConnectServer(_bstr_t(L"ROOT\\CIMV2"), NULL, NULL, NULL, 0, NULL, NULL, pService);

  if (FAILED(result))
  {
    (*pLocator)->Release();

    return result;
  }

  result = CoSetProxyBlanket(*pService, RPC_C_AUTHN_WINNT, RPC_C_AUTHZ_NONE, NULL, RPC_C_AUTHN_LEVEL_CALL, RPC_C_IMP_LEVEL_IMPERSONATE, NULL, EOAC_NONE);

  if (FAILED(result))
  {
    (*pService)->Release();
    (*pLocator)->Release();

    return result;
  }
  return NOERROR;
}

template <typename T>
HRESULT QueryValue(IWbemServices* pService, const wchar_t* query, const wchar_t* propertyName, T* propertyValue, size_t maximumPropertyValueLength)
{
  IEnumWbemClassObject* pEnumerator = NULL;
  HRESULT result = pService->ExecQuery(bstr_t(L"WQL"), bstr_t(query), WBEM_FLAG_FORWARD_ONLY | WBEM_FLAG_RETURN_IMMEDIATELY, NULL, &pEnumerator);

  if (FAILED(result))
    return result;

  IWbemClassObject *pQueryObject = NULL;
  while (pEnumerator)
  {
    try
    {
      ULONG returnedObjectCount = 0;
      result = pEnumerator->Next(WBEM_INFINITE, 1, &pQueryObject, &returnedObjectCount);
      if (returnedObjectCount == 0)
        break;

      VARIANT objectProperty;
      result = pQueryObject->Get(propertyName, 0, &objectProperty, 0, 0);
      if (FAILED(result))
      {
        if (pEnumerator != NULL)
          pEnumerator->Release();

        if (pQueryObject != NULL)
          pQueryObject->Release();

        return result;
      }

      if ((objectProperty.vt & VT_BSTR) == VT_BSTR)
      {
        _tcscpy_s((TCHAR *)propertyValue, maximumPropertyValueLength, objectProperty.bstrVal);
        break;
      }
      if ((objectProperty.vt & VT_I4) == VT_I4)
      {
        assert(maximumPropertyValueLength >= sizeof(INT32));
        *(INT32 *)propertyValue = objectProperty.intVal;
        break;
      }
      VariantClear(&objectProperty);
    }
    catch (...)
    {
      if (pEnumerator != NULL)
        pEnumerator->Release();

      if (pQueryObject != NULL)
        pQueryObject->Release();

      return NOERROR;
    }
  }

  if (pEnumerator != NULL)
    pEnumerator->Release();

  if (pQueryObject != NULL)
    pQueryObject->Release();

  return NOERROR;
}

HRESULT GetCpuCoresCount(INT32 *pCoresCount)
{
  HRESULT result = InitializeCom();
  if (FAILED(result))
    return result;

  IWbemLocator* pLocator = NULL;
  IWbemServices* pService = NULL;
  result = GetWbemService(&pLocator, &pService);
  if (FAILED(result))
  {
    CoUninitialize();
    return result;
  }

  result = QueryValue(pService, L"SELECT NumberOfCores FROM Win32_Processor", L"NumberOfCores", pCoresCount, sizeof(*pCoresCount));
  
  if (FAILED(result))
  {
    pService->Release();
    pLocator->Release();
    CoUninitialize();

    return result;
  }
  pService->Release();
  pLocator->Release();
  CoUninitialize();
  return NOERROR;
}

size_t GetCpuCoresCount()
{
  INT32 nCoresCount;
  if (FAILED(GetCpuCoresCount(&nCoresCount)))
  {
    return 0;
  }
  return nCoresCount;
}
