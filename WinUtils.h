#pragma once


size_t GetCpuCoresCount();
void StartCounter();
double GetCounter();
bool IsAVXSupported();
