// ArrayAverage.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <assert.h>
#include <immintrin.h>
#include <conio.h>
#include <memory>
#include <future>
#include <thread>
#include "ThreadPool.h"
#include "WinUtils.h"

const size_t ds = 500001;

float avgSimple(float const *pF, size_t s)
{
  float result = 0.f;
  assert(pF != NULL && s > 0); //  "invalid input"
  size_t i = 0;
  float div = 1.0f / float(s);
  for (; i < s; i++)
  {
    result += pF[i];
  }
  return result * div;
}

float avgPrecise(float const *pF, size_t s)
{
  float result = 0.f;
  assert(pF != NULL && s > 0); //  "invalid input"
  size_t i = 0;
  float div = 1.0f / float(s);
  for (; i < s; i++)
  {
    result += div * pF[i];
  }
  return result;
}

float avgAVXSimple(float const *pF, size_t s)
{
  float result = 0.f;
  assert(pF != NULL && s > 0); //  "invalid input"
  assert(((intptr_t)pF & 3) == 0); //  "Data is not aligned"
  size_t i = 0;
  for (; i < s; i++)
  {
    if (((intptr_t)(pF + i) & 0x1F) == 0)
      break;
    result += pF[i];
  }
  size_t elemsLeft = s - i;
  size_t nVecs = elemsLeft / 8;
  float const *p = pF + i;
  float const *pEnd = p + nVecs * 8;
  float mmRes = 0.0f;
  if (p < pEnd)
  {
    __m256 acc;
    __m256 b;
    acc = _mm256_load_ps(p);
    p += 8;
    while (p < pEnd)
    {
      b = _mm256_load_ps(p);
      acc = _mm256_add_ps(b, acc);
      p += 8;
    }
    acc = _mm256_hadd_ps(acc, acc);
    __m128 acch = _mm256_extractf128_ps(acc, 1);
    __m128 accl = _mm256_castps256_ps128(acc);
    acch = _mm_hadd_ps(acch, acch);
    accl = _mm_hadd_ps(accl, accl);
    _mm_store_ss(&mmRes, acch);
    result += mmRes;
    _mm_store_ss(&mmRes, accl);
    result += mmRes;
    _mm256_zeroall();
  }
  size_t nLast = elemsLeft % 8;
  for (i = s - nLast; i < s; i++)
  {
    result += pF[i];
  }
  return result / float(s);
}

float avgFastPrec(float const *pF, size_t s)
{
  float result = 0.f;
  assert(pF != NULL && s > 0); //  "invalid input"
  assert(((intptr_t)pF & 3) == 0); //  "Data is not aligned"
  size_t i = 0;
  float recSize = 1.0f / float(s);
  //process header
  for (; i < s; i++)
  {
    if (((intptr_t)(pF + i) & 0x1F) == 0)
      break;
    result += recSize * pF[i];
  }
  size_t elemsLeft = s - i;
  size_t nVecs = elemsLeft / 8;
  float const *p = pF + i;
  float const *pEnd = p + nVecs * 8;
  float mmRes = 0.0f;
  if (p < pEnd)
  {
    //main loop preparation
    __m256 acc; //accumulator
    __m256 b;   //run register
    __m256 div; // divider
    div = _mm256_broadcast_ss(&recSize);
    acc = _mm256_load_ps(p);
    p += 8;
    _mm_prefetch((char const *)p, _MM_HINT_T0);
    acc = _mm256_mul_ps(acc, div);
    while (p < pEnd)  //main loop
    {
      b = _mm256_load_ps(p);
      p += 8;
      _mm_prefetch((char const *)p, _MM_HINT_T0);
      b = _mm256_mul_ps(b, div);
      acc = _mm256_add_ps(b, acc);
    }
    //YMM registers reduce
    acc = _mm256_hadd_ps(acc, acc);
    __m128 acch = _mm256_extractf128_ps(acc, 1);
    __m128 accl = _mm256_castps256_ps128(acc);
    acch = _mm_hadd_ps(acch, acch);
    accl = _mm_hadd_ps(accl, accl);
    _mm_store_ss(&mmRes, acch);
    result += mmRes;
    _mm_store_ss(&mmRes, accl);
    result += mmRes;
    _mm256_zeroall(); //compiler expect YMMs set to zero
  }
  //tail processing
  size_t nLast = elemsLeft % 8;
  for (i = s - nLast; i < s; i++)
  {
    result += recSize * pF[i];
  }
  return result;
}

float avgXMMPrec(float const *pF, size_t s)
{
  float result = 0.f;
  assert(pF != NULL && s > 0); //  "invalid input"
  assert(((intptr_t)pF & 3) == 0); //  "Data is not aligned"
  size_t i = 0;
  float recSize = 1.0f / float(s);
  //process header
  for (; i < s; i++)
  {
    if (((intptr_t)(pF + i) & 0x0F) == 0)
      break;
    result += recSize * pF[i];
  }
  size_t elemsLeft = s - i;
  size_t nVecs = elemsLeft / 4;
  float const *p = pF + i;
  float const *pEnd = p + nVecs * 4;
  float mmRes = 0.0f;
  if (p < pEnd)
  {
    //main loop preparation
    __m128 acc; //accumulator
    __m128 b;   //run register
    __m128 div; // divider
    div = _mm_load_ps1(&recSize);
    acc = _mm_load_ps(p);
    p += 4;
    _mm_prefetch((char const *)p, _MM_HINT_T0);
    acc = _mm_mul_ps(acc, div);
    while (p < pEnd)  //main loop
    {
      b = _mm_load_ps(p);
      p += 4;
      _mm_prefetch((char const *)p, _MM_HINT_T0);
      b = _mm_mul_ps(b, div);
      acc = _mm_add_ps(b, acc);
    }
    //XMM registers reduce
    acc = _mm_hadd_ps(acc, acc);
    acc = _mm_hadd_ps(acc, acc);
    _mm_store_ss(&mmRes, acc);
    result += mmRes;
  }
  //tail processing
  size_t nLast = elemsLeft % 4;
  for (i = s - nLast; i < s; i++)
  {
    result += recSize * pF[i];
  }
  return result;
}

template <typename F>
float avgPreciseThreaded(float const *pF, size_t s, ThreadPool &pool, F f)
{
  size_t numThreads = pool.getThreadsCount();
  size_t partSize = s / numThreads;
  size_t i;
  //std::unique_ptr<std::future<float>[]> threads(new std::future<float>[numThreads]);
  auto * p = (std::future<float>*)_alloca(sizeof(std::future<float>) * numThreads + sizeof(intptr_t));
  std::future<float> *threads = new (p) std::future<float>[numThreads];
  size_t lastChunkSize = 0;
  for (i = 0; i < numThreads; i++)
  {
    size_t nSize = partSize;
    if (i == (numThreads - 1))
    {
      nSize += s - partSize * numThreads;
      lastChunkSize = nSize;
    }
    threads[i] = pool.enqueue([pF, i, partSize, nSize, f]()
    {
      return f(pF + i * partSize, nSize);
    });
  }
  
  float result = 0;
  float divider = 1.0f / float(numThreads - 1);
  for (i = 0; i < numThreads - 1; i++)
  {
    threads[i].wait();
    result += divider * threads[i].get();
  }
  threads[numThreads - 1].wait();

  //correct result
  result = result / float(s) * partSize * (numThreads - 1) + threads[numThreads - 1].get() / float(s) * lastChunkSize;

  return result;
}


int _tmain(int argc, _TCHAR* argv[])
{
  std::unique_ptr<float[]> pf(new float[ds]);
  for (int i = 0; i < ds; i++)
  {
    pf[i] = float(i + 1);
  }

  bool avxWorks = IsAVXSupported();
   
  printf("Correct solution: %f\n", (pf[0] + pf[ds - 1]) / 2.0);
  float r = 0;
  double dt = 0.0;
  
  StartCounter();
  r = avgSimple(pf.get(), ds);
  dt = GetCounter();
  printf("Simple algorithm: %g, time: %gms\n", r, dt);
  StartCounter();
  r = avgPrecise(pf.get(), ds);
  dt = GetCounter();
  printf("Precise algorithm: %g, time: %gms\n", r, dt);
  StartCounter();
  r = avgXMMPrec(pf.get(), ds);
  dt = GetCounter();
  printf("Precise XMM algorithm: %g, time: %gms\n", r, dt);
  if (avxWorks)
  {
    StartCounter();
    r = avgAVXSimple(pf.get(), ds);
    dt = GetCounter();
    printf("Fast AVX algorithm: %g, time: %gms\n", r, dt);
  }
  if (avxWorks)
  {
    StartCounter();
    r = avgFastPrec(pf.get(), ds);
    dt = GetCounter();
    printf("Precise & fast AVX algorithm: %g, time: %gms\n", r, dt);
  }
  size_t numThreads = GetCpuCoresCount();
  if(numThreads == (size_t)-1 || numThreads == 0)
    numThreads = std::thread::hardware_concurrency();

  ThreadPool pool(numThreads);
  StartCounter();
  r = avgPreciseThreaded(pf.get(), ds, pool, avgPrecise);
  dt = GetCounter();
  printf("Precise threaded algorithm: %g, time: %gms\n", r, dt);
  StartCounter();
  r = avgPreciseThreaded(pf.get(), ds, pool, avgXMMPrec);
  dt = GetCounter();
  printf("Precise XMM threaded algorithm: %g, time: %gms\n", r, dt);
  if (avxWorks)
  {
    StartCounter();
    r = avgPreciseThreaded(pf.get(), ds, pool, avgFastPrec);
    dt = GetCounter();
    printf("Precise & fast AVX threaded algorithm: %g, time: %gms\n", r, dt);
  }
  _getch();
  return 0;
}

